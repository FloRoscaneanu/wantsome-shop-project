package users_credentials;

public class UserData {
    private String username;
    private String email;
    private String passw1;
    private String passw2;

    public UserData(String username, String email, String passw1, String passw2) {
        this.username = username;
        this.email = email;
        this.passw1 = passw1;
        this.passw2 = passw2;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassw1() {
        return passw1;
    }

    public String getPassw2() {
        return passw2;
    }

    public static UserCreat builder() {
        return new UserCreat();
    }

    public static class UserCreat {
        private String username;
        private String email;
        private String passw1;
        private String passw2;

        public UserCreat setUsername(String username) {
            this.username = username;
            return this;
        }

        public UserCreat setEmail(String email) {
            this.email = email;
            return this;
        }

        public UserCreat setPassw1(String passw1) {
            this.passw1 = passw1;
            return this;
        }

        public UserCreat setPassw2(String passw2) {
            this.passw2 = passw2;
            return this;
        }

        public UserData build() {
            return new UserData(username, email, passw1, passw2);
        }
    }
}

