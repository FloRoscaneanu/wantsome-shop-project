package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BlogPage {
    private WebDriver driver;
    private final By BLOG_TITLE = By.xpath("//h2[text()='Blog']");
    private final By SELECT_WATCH_BTN = By.xpath("//a[text()='Gold Watch']");
    private final By GLD_WTCH_TITLE = By.xpath("//*[@id=\"content\"]/div/div/h2");
    private final By REPLY_BTN = By.xpath("//*[@id=\"div-comment-285\"]/div[2]/a");
    private final By POST_BTN = By.id("submit");
    private final By ERROR_PAGE_MSG = By.xpath("//*[@id=\"error-page\"]/div/p/strong");
    private final By USER_FIELD = By.xpath("//*[@id=\"author\"]");
    private final By EMAIL_FIELD = By.xpath("//*[@id=\"email\"]");
    private final By POST_TEXT = By.xpath("//*[@id=\"div-comment-5843\"]/div[1]/p");
    private final By COMMENT_AREA = By.id("comment");
    private final By COMMENT_CONTENT = By.className("comment-content");

    public BlogPage(WebDriver driver) {

        this.driver = driver;
    }

    public String getBlogTitle() {
        return driver.findElement(BLOG_TITLE).getText();
    }

    public BlogPage clickGoldWatchBtn() {
        driver.findElement(SELECT_WATCH_BTN).click();
        return new BlogPage(driver);
    }

    public String getGldWtchTitle() {
        return driver.findElement(GLD_WTCH_TITLE).getText();
    }

    public BlogPage clickReplyBtn() {
        driver.findElement(REPLY_BTN).click();
        return new BlogPage(driver);
    }

    public long time = System.currentTimeMillis();

    public BlogPage writeReplyToComment(String comment) {

        driver.findElement(COMMENT_AREA).sendKeys(comment);
//        WebDriverWait webDriverWait = new WebDriverWait(driver,5);
//        driver.findElement(POST_BTN).click();
        return new BlogPage(driver);
    }

    public String getErrorText() {
        return driver.findElement(ERROR_PAGE_MSG).getText();
    }

    public String getReplyText() {
        return driver.findElement(POST_TEXT).getText();
    }

    public BlogPage clickPostBtn() {
        driver.findElement(POST_BTN).click();
        return new BlogPage(driver);
    }

    public BlogPage insertUser() {
        String user = "John";
        driver.findElement(USER_FIELD).sendKeys(user);
        return new BlogPage(driver);
    }

    public BlogPage insertEmail() {
        String email = "John@gmail.com";
        driver.findElement(EMAIL_FIELD).sendKeys(email);
        return new BlogPage(driver);
    }

    public List<WebElement> allReplies() {
        List<WebElement> myList = driver.findElements(COMMENT_CONTENT);
        {
            for (WebElement elements : myList) {
               // myList.add(elements);
                System.out.println(elements);
            }
            //System.out.println(myList);
            return myList;
        }

    }

    }

