package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage {
    private final By PAGE_TITLE = By.tagName("h2");
    private final By EMPTY_CART_TEXT = By.xpath("//p[contains(@class, 'cart-empty')]");
    private final By CART_VALUE = By.className("cart-value");

    private WebDriver driver;

    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getTitle(){
        return driver.findElement(PAGE_TITLE).getText();
    }

    public String getEmptyCartText(){
        return  driver.findElement(EMPTY_CART_TEXT).getText();
    }

    public int getCartValue() {
        return Integer.parseInt(driver.findElement(CART_VALUE).getText());
    }
}

