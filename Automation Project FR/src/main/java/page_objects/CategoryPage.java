package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CategoryPage {
    WebDriver driver;
    private final By CATEGORY_TITLE = By.className("entry-title");
    private final By CART_ICON = By.className("add_to_cart_button");
    private final By PRODUCT_IMAGE = By.className("products-img");

    public CategoryPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getCategoryTitle() {
        return driver.findElement(CATEGORY_TITLE).getText();
    }

    public void addToChart(){
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(PRODUCT_IMAGE)).build().perform();
        WebDriverWait webDriverWait = new WebDriverWait(driver,5);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(CART_ICON));
        driver.findElement(CART_ICON).click();
    }
}
