package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class ShopHomePage {
    private final WebDriver driver;
    private final By CART_BTN = By.xpath("//a[text()='Cart']");
    private final By CATEGORY_BTN = By.className("category-toggle");
    private final By MEN_COLLECTION_BTN = By.xpath("//a[text()='Men Collection']");
    private final By SEARCH_ICON = By.className("search-icon");
    private final By SEARCH_FIELD = By.className("search-field");
    private final By BLOG_BTN = By.xpath("//a[text()='Blog']");


    public ShopHomePage(WebDriver webDriver) {
        this.driver = webDriver;
        //metoda prin care se identifica un webelement
    }

    public CartPage navigateToCart() {
        driver.findElement(CART_BTN).click();
        return new CartPage(driver);
        //metoda prin care se cauta un element
    }

    public void clickOnCategory(){
        driver.findElement(CATEGORY_BTN).click();
    }

    public CategoryPage clickOnMenCollection(){
        driver.findElement(MEN_COLLECTION_BTN).click();
        return new CategoryPage(driver);
    }

    public void clickOnSearchIcon(){
        driver.findElement(SEARCH_ICON).click();
    }

    public SearchResultPage typeSearch(String searchInput){
        driver.findElement(SEARCH_FIELD).sendKeys(searchInput);
        driver.findElement(SEARCH_FIELD).sendKeys(Keys.ENTER);
        return new SearchResultPage(driver);
    }
    public BlogPage clickOnBlog(){
        driver.findElement(BLOG_BTN).click();
        return new BlogPage(driver);
    }

}

