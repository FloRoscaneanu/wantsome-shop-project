package category;

import org.junit.Assert;
import org.junit.Test;
import page_objects.CategoryPage;
import page_objects.ShopHomePage;
import utils.BaseTestClass;

public class CategoryTests extends BaseTestClass{
    @Test
    public void checkMenTitle(){
        ShopHomePage shopHomePage = new ShopHomePage(driver);
        shopHomePage.clickOnCategory();
        CategoryPage menCollection = shopHomePage.clickOnMenCollection();
        Assert.assertEquals("Men Collection", menCollection.getCategoryTitle());
    }
}
