package cart;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import page_objects.CartPage;
import page_objects.CategoryPage;
import page_objects.ShopHomePage;
import utils.BaseTestClass;

public class CartTests extends BaseTestClass {

    @Test
    public void checkEmptyCart(){
        //click on Cart tab
        driver.findElement(By.xpath("//a[text()='Cart']")).click();

        //check Cart page title
        Assert.assertEquals("CART", driver.findElement(By.className("entry-title")).getText());

        //check Cart page message
        Assert.assertEquals("Your cart is currently empty.",driver.findElement(By.className("cart-empty")).getText());
    }

    @Test
    public void checkEmptyCartPageObject(){
        CartPage cartPage = new ShopHomePage(driver).navigateToCart(); //se creeaza un obiect nou de tip cartPage
        Assert.assertEquals("CART", cartPage.getTitle());
        Assert.assertEquals("Your cart is currently empty.", cartPage.getEmptyCartText());
    }

//    @Test
//    public void checkCartValue(){
//        ShopHomePage shopHomePage = new ShopHomePage(driver);
//        shopHomePage.clickOnCategory();
//        CategoryPage categoryPage = shopHomePage.clickOnMenCollection();
//        categoryPage.addToChart();
//        CartPage cartPage = new CartPage(driver);
//        Assert.assertEquals(1, cartPage.getCartValue());
//
//    }
}

