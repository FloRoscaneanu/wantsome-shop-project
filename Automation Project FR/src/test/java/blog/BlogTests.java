package blog;

import org.apache.xpath.operations.Bool;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import page_objects.BlogPage;
import page_objects.SearchResultPage;
import page_objects.ShopHomePage;
import utils.BaseTestClass;

import java.util.List;

public class BlogTests extends BaseTestClass {

    @Test
    //Verificam titlul paginii Blog
    public void checkBlogPageTitle() {
        goToHomeShopPage();
        BlogPage blogPage = new BlogPage(driver);
        blogPage.getBlogTitle();
        Assert.assertEquals("BLOG", blogPage.getBlogTitle());
    }

    @Test
    //verificam titlul paginii Gold Watch
    public void clickGldWtchBreadcrumb(){
        goToHomeShopPage();
        BlogPage blogPage = new BlogPage(driver);
        blogPage.clickGoldWatchBtn();
        Assert.assertEquals("GOLD WATCH", blogPage.getGldWtchTitle());
    }

    @Test
    //verificam daca fara User si Parola este afisata EROARE
    public void postReplyToCommentError() {
        goToHomeShopPage();
        BlogPage blogPage = new BlogPage(driver);
        String comment = "This is a test comment" + blogPage.time;
        blogPage.clickGoldWatchBtn();
        blogPage.clickReplyBtn();
        blogPage.writeReplyToComment(comment);
        blogPage.clickPostBtn();
        Assert.assertEquals("ERROR", blogPage.getErrorText());
    }

    @Test
    //verificam daca un reply este afisat
    public void postReplyToComment() throws InterruptedException {
        goToHomeShopPage();
        BlogPage blogPage = new BlogPage(driver);
        String comment = "This is a test comment" + blogPage.time;
        blogPage.clickGoldWatchBtn();
        blogPage.clickReplyBtn();
        Thread.sleep(300);
        blogPage.writeReplyToComment(comment);
        blogPage.insertEmail();
        blogPage.insertUser();
        blogPage.clickPostBtn();
        List<WebElement> replies = blogPage.allReplies();
//        String expectedComment = "This is a test comment" + blogPage.time;
        boolean found = false;
        for (WebElement reply : replies) {
            String replyText = reply.getText();
            System.out.println(replyText);
            if(replyText.equals(comment)){
                found = true;
            }
        }
        Assert.assertTrue(found);
    }

    private void goToHomeShopPage() {
        ShopHomePage shopHomePage = new ShopHomePage(driver);
        shopHomePage.clickOnBlog();
    }
}
