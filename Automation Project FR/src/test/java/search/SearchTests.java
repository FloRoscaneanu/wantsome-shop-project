package search;

import org.junit.Assert;
import org.junit.Test;
import page_objects.SearchResultPage;
import page_objects.ShopHomePage;
import utils.BaseTestClass;

public class SearchTests extends BaseTestClass {
    @Test
    public void checkSearchResults(){
        ShopHomePage shopHomePage = new ShopHomePage(driver);
        shopHomePage.clickOnSearchIcon();
        SearchResultPage searchResults = shopHomePage.typeSearch("watch");
        Assert.assertTrue(searchResults.checkResultsContainSearch("watch"));
    }
}
